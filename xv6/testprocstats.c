#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
    int running, runnable, sleeping, zombie;
    procstats(&running, &runnable, &sleeping, &zombie);
    printf(1, "Running: %d, runnable: %d, sleeping: %d, zombie: %d\n", running, runnable, sleeping, zombie);
    int pid = fork();
    if(pid == 0){
      sleep(100);
    } else {
      kill(pid);
      sleep(10);
      printf(1, "%s", "Creating zombie child...\n");
      procstats(&running, &runnable, &sleeping, &zombie);
      printf(1, "Running: %d, runnable: %d, sleeping: %d, zombie: %d\n", running, runnable, sleeping, zombie);

    }
  exit();
}
