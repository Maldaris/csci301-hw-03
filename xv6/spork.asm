
_spork:     file format elf32-i386


Disassembly of section .text:

00000000 <waitForInput>:
#include "types.h"
#include "user.h"
#include "stat.h"


int waitForInput(char* i){
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	81 ec 28 02 00 00    	sub    $0x228,%esp
  char ebuf[512];
  char *e = gets(ebuf, 10);
   9:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
  10:	00 
  11:	8d 85 f4 fd ff ff    	lea    -0x20c(%ebp),%eax
  17:	89 04 24             	mov    %eax,(%esp)
  1a:	e8 a2 01 00 00       	call   1c1 <gets>
  1f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(strcmp(e, i) == 0){
  22:	8b 45 08             	mov    0x8(%ebp),%eax
  25:	89 44 24 04          	mov    %eax,0x4(%esp)
  29:	8b 45 f4             	mov    -0xc(%ebp),%eax
  2c:	89 04 24             	mov    %eax,(%esp)
  2f:	e8 d0 00 00 00       	call   104 <strcmp>
  34:	85 c0                	test   %eax,%eax
  36:	75 07                	jne    3f <waitForInput+0x3f>
    return 1;
  38:	b8 01 00 00 00       	mov    $0x1,%eax
  3d:	eb 0b                	jmp    4a <waitForInput+0x4a>
  } else {
    return waitForInput(i);
  3f:	8b 45 08             	mov    0x8(%ebp),%eax
  42:	89 04 24             	mov    %eax,(%esp)
  45:	e8 b6 ff ff ff       	call   0 <waitForInput>
  }
}
  4a:	c9                   	leave  
  4b:	c3                   	ret    

0000004c <main>:

int main(int argc, char** argv){
  4c:	55                   	push   %ebp
  4d:	89 e5                	mov    %esp,%ebp
  4f:	83 e4 f0             	and    $0xfffffff0,%esp
  52:	83 ec 20             	sub    $0x20,%esp
  while(1){
    waitForInput("go\n");
  55:	c7 04 24 5b 08 00 00 	movl   $0x85b,(%esp)
  5c:	e8 9f ff ff ff       	call   0 <waitForInput>
    int pid = fork();
  61:	e8 a2 02 00 00       	call   308 <fork>
  66:	89 44 24 1c          	mov    %eax,0x1c(%esp)
    if(pid == 0){
  6a:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
  6f:	75 16                	jne    87 <main+0x3b>
      char *nargs[] = {};
      exec("itsaspork", nargs);
  71:	8d 44 24 1c          	lea    0x1c(%esp),%eax
  75:	89 44 24 04          	mov    %eax,0x4(%esp)
  79:	c7 04 24 5f 08 00 00 	movl   $0x85f,(%esp)
  80:	e8 c3 02 00 00       	call   348 <exec>
      kill(pid);
      wait();
      exit();
      return 0;
    }
  }
  85:	eb ce                	jmp    55 <main+0x9>
    int pid = fork();
    if(pid == 0){
      char *nargs[] = {};
      exec("itsaspork", nargs);
    } else {
      waitForInput("stop\n");
  87:	c7 04 24 69 08 00 00 	movl   $0x869,(%esp)
  8e:	e8 6d ff ff ff       	call   0 <waitForInput>
      kill(pid);
  93:	8b 44 24 1c          	mov    0x1c(%esp),%eax
  97:	89 04 24             	mov    %eax,(%esp)
  9a:	e8 a1 02 00 00       	call   340 <kill>
      wait();
  9f:	e8 74 02 00 00       	call   318 <wait>
      exit();
  a4:	e8 67 02 00 00       	call   310 <exit>
  a9:	90                   	nop
  aa:	90                   	nop
  ab:	90                   	nop

000000ac <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  ac:	55                   	push   %ebp
  ad:	89 e5                	mov    %esp,%ebp
  af:	57                   	push   %edi
  b0:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  b1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  b4:	8b 55 10             	mov    0x10(%ebp),%edx
  b7:	8b 45 0c             	mov    0xc(%ebp),%eax
  ba:	89 cb                	mov    %ecx,%ebx
  bc:	89 df                	mov    %ebx,%edi
  be:	89 d1                	mov    %edx,%ecx
  c0:	fc                   	cld    
  c1:	f3 aa                	rep stos %al,%es:(%edi)
  c3:	89 ca                	mov    %ecx,%edx
  c5:	89 fb                	mov    %edi,%ebx
  c7:	89 5d 08             	mov    %ebx,0x8(%ebp)
  ca:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  cd:	5b                   	pop    %ebx
  ce:	5f                   	pop    %edi
  cf:	5d                   	pop    %ebp
  d0:	c3                   	ret    

000000d1 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  d1:	55                   	push   %ebp
  d2:	89 e5                	mov    %esp,%ebp
  d4:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  d7:	8b 45 08             	mov    0x8(%ebp),%eax
  da:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  dd:	8b 45 0c             	mov    0xc(%ebp),%eax
  e0:	0f b6 10             	movzbl (%eax),%edx
  e3:	8b 45 08             	mov    0x8(%ebp),%eax
  e6:	88 10                	mov    %dl,(%eax)
  e8:	8b 45 08             	mov    0x8(%ebp),%eax
  eb:	0f b6 00             	movzbl (%eax),%eax
  ee:	84 c0                	test   %al,%al
  f0:	0f 95 c0             	setne  %al
  f3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  f7:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  fb:	84 c0                	test   %al,%al
  fd:	75 de                	jne    dd <strcpy+0xc>
    ;
  return os;
  ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 102:	c9                   	leave  
 103:	c3                   	ret    

00000104 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 104:	55                   	push   %ebp
 105:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 107:	eb 08                	jmp    111 <strcmp+0xd>
    p++, q++;
 109:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 111:	8b 45 08             	mov    0x8(%ebp),%eax
 114:	0f b6 00             	movzbl (%eax),%eax
 117:	84 c0                	test   %al,%al
 119:	74 10                	je     12b <strcmp+0x27>
 11b:	8b 45 08             	mov    0x8(%ebp),%eax
 11e:	0f b6 10             	movzbl (%eax),%edx
 121:	8b 45 0c             	mov    0xc(%ebp),%eax
 124:	0f b6 00             	movzbl (%eax),%eax
 127:	38 c2                	cmp    %al,%dl
 129:	74 de                	je     109 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 12b:	8b 45 08             	mov    0x8(%ebp),%eax
 12e:	0f b6 00             	movzbl (%eax),%eax
 131:	0f b6 d0             	movzbl %al,%edx
 134:	8b 45 0c             	mov    0xc(%ebp),%eax
 137:	0f b6 00             	movzbl (%eax),%eax
 13a:	0f b6 c0             	movzbl %al,%eax
 13d:	89 d1                	mov    %edx,%ecx
 13f:	29 c1                	sub    %eax,%ecx
 141:	89 c8                	mov    %ecx,%eax
}
 143:	5d                   	pop    %ebp
 144:	c3                   	ret    

00000145 <strlen>:

uint
strlen(char *s)
{
 145:	55                   	push   %ebp
 146:	89 e5                	mov    %esp,%ebp
 148:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 14b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 152:	eb 04                	jmp    158 <strlen+0x13>
 154:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 158:	8b 45 fc             	mov    -0x4(%ebp),%eax
 15b:	03 45 08             	add    0x8(%ebp),%eax
 15e:	0f b6 00             	movzbl (%eax),%eax
 161:	84 c0                	test   %al,%al
 163:	75 ef                	jne    154 <strlen+0xf>
    ;
  return n;
 165:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 168:	c9                   	leave  
 169:	c3                   	ret    

0000016a <memset>:

void*
memset(void *dst, int c, uint n)
{
 16a:	55                   	push   %ebp
 16b:	89 e5                	mov    %esp,%ebp
 16d:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 170:	8b 45 10             	mov    0x10(%ebp),%eax
 173:	89 44 24 08          	mov    %eax,0x8(%esp)
 177:	8b 45 0c             	mov    0xc(%ebp),%eax
 17a:	89 44 24 04          	mov    %eax,0x4(%esp)
 17e:	8b 45 08             	mov    0x8(%ebp),%eax
 181:	89 04 24             	mov    %eax,(%esp)
 184:	e8 23 ff ff ff       	call   ac <stosb>
  return dst;
 189:	8b 45 08             	mov    0x8(%ebp),%eax
}
 18c:	c9                   	leave  
 18d:	c3                   	ret    

0000018e <strchr>:

char*
strchr(const char *s, char c)
{
 18e:	55                   	push   %ebp
 18f:	89 e5                	mov    %esp,%ebp
 191:	83 ec 04             	sub    $0x4,%esp
 194:	8b 45 0c             	mov    0xc(%ebp),%eax
 197:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 19a:	eb 14                	jmp    1b0 <strchr+0x22>
    if(*s == c)
 19c:	8b 45 08             	mov    0x8(%ebp),%eax
 19f:	0f b6 00             	movzbl (%eax),%eax
 1a2:	3a 45 fc             	cmp    -0x4(%ebp),%al
 1a5:	75 05                	jne    1ac <strchr+0x1e>
      return (char*)s;
 1a7:	8b 45 08             	mov    0x8(%ebp),%eax
 1aa:	eb 13                	jmp    1bf <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 1ac:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1b0:	8b 45 08             	mov    0x8(%ebp),%eax
 1b3:	0f b6 00             	movzbl (%eax),%eax
 1b6:	84 c0                	test   %al,%al
 1b8:	75 e2                	jne    19c <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 1ba:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1bf:	c9                   	leave  
 1c0:	c3                   	ret    

000001c1 <gets>:

char*
gets(char *buf, int max)
{
 1c1:	55                   	push   %ebp
 1c2:	89 e5                	mov    %esp,%ebp
 1c4:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1c7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1ce:	eb 44                	jmp    214 <gets+0x53>
    cc = read(0, &c, 1);
 1d0:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 1d7:	00 
 1d8:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1db:	89 44 24 04          	mov    %eax,0x4(%esp)
 1df:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1e6:	e8 3d 01 00 00       	call   328 <read>
 1eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 1ee:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1f2:	7e 2d                	jle    221 <gets+0x60>
      break;
    buf[i++] = c;
 1f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1f7:	03 45 08             	add    0x8(%ebp),%eax
 1fa:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 1fe:	88 10                	mov    %dl,(%eax)
 200:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 204:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 208:	3c 0a                	cmp    $0xa,%al
 20a:	74 16                	je     222 <gets+0x61>
 20c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 210:	3c 0d                	cmp    $0xd,%al
 212:	74 0e                	je     222 <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 214:	8b 45 f0             	mov    -0x10(%ebp),%eax
 217:	83 c0 01             	add    $0x1,%eax
 21a:	3b 45 0c             	cmp    0xc(%ebp),%eax
 21d:	7c b1                	jl     1d0 <gets+0xf>
 21f:	eb 01                	jmp    222 <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 221:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 222:	8b 45 f0             	mov    -0x10(%ebp),%eax
 225:	03 45 08             	add    0x8(%ebp),%eax
 228:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 22b:	8b 45 08             	mov    0x8(%ebp),%eax
}
 22e:	c9                   	leave  
 22f:	c3                   	ret    

00000230 <stat>:

int
stat(char *n, struct stat *st)
{
 230:	55                   	push   %ebp
 231:	89 e5                	mov    %esp,%ebp
 233:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 236:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 23d:	00 
 23e:	8b 45 08             	mov    0x8(%ebp),%eax
 241:	89 04 24             	mov    %eax,(%esp)
 244:	e8 07 01 00 00       	call   350 <open>
 249:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 24c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 250:	79 07                	jns    259 <stat+0x29>
    return -1;
 252:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 257:	eb 23                	jmp    27c <stat+0x4c>
  r = fstat(fd, st);
 259:	8b 45 0c             	mov    0xc(%ebp),%eax
 25c:	89 44 24 04          	mov    %eax,0x4(%esp)
 260:	8b 45 f0             	mov    -0x10(%ebp),%eax
 263:	89 04 24             	mov    %eax,(%esp)
 266:	e8 fd 00 00 00       	call   368 <fstat>
 26b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 26e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 271:	89 04 24             	mov    %eax,(%esp)
 274:	e8 bf 00 00 00       	call   338 <close>
  return r;
 279:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 27c:	c9                   	leave  
 27d:	c3                   	ret    

0000027e <atoi>:

int
atoi(const char *s)
{
 27e:	55                   	push   %ebp
 27f:	89 e5                	mov    %esp,%ebp
 281:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 284:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 28b:	eb 24                	jmp    2b1 <atoi+0x33>
    n = n*10 + *s++ - '0';
 28d:	8b 55 fc             	mov    -0x4(%ebp),%edx
 290:	89 d0                	mov    %edx,%eax
 292:	c1 e0 02             	shl    $0x2,%eax
 295:	01 d0                	add    %edx,%eax
 297:	01 c0                	add    %eax,%eax
 299:	89 c2                	mov    %eax,%edx
 29b:	8b 45 08             	mov    0x8(%ebp),%eax
 29e:	0f b6 00             	movzbl (%eax),%eax
 2a1:	0f be c0             	movsbl %al,%eax
 2a4:	8d 04 02             	lea    (%edx,%eax,1),%eax
 2a7:	83 e8 30             	sub    $0x30,%eax
 2aa:	89 45 fc             	mov    %eax,-0x4(%ebp)
 2ad:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 2b1:	8b 45 08             	mov    0x8(%ebp),%eax
 2b4:	0f b6 00             	movzbl (%eax),%eax
 2b7:	3c 2f                	cmp    $0x2f,%al
 2b9:	7e 0a                	jle    2c5 <atoi+0x47>
 2bb:	8b 45 08             	mov    0x8(%ebp),%eax
 2be:	0f b6 00             	movzbl (%eax),%eax
 2c1:	3c 39                	cmp    $0x39,%al
 2c3:	7e c8                	jle    28d <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 2c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2c8:	c9                   	leave  
 2c9:	c3                   	ret    

000002ca <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2ca:	55                   	push   %ebp
 2cb:	89 e5                	mov    %esp,%ebp
 2cd:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2d0:	8b 45 08             	mov    0x8(%ebp),%eax
 2d3:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 2d6:	8b 45 0c             	mov    0xc(%ebp),%eax
 2d9:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 2dc:	eb 13                	jmp    2f1 <memmove+0x27>
    *dst++ = *src++;
 2de:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2e1:	0f b6 10             	movzbl (%eax),%edx
 2e4:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2e7:	88 10                	mov    %dl,(%eax)
 2e9:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 2ed:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2f1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 2f5:	0f 9f c0             	setg   %al
 2f8:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 2fc:	84 c0                	test   %al,%al
 2fe:	75 de                	jne    2de <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 300:	8b 45 08             	mov    0x8(%ebp),%eax
}
 303:	c9                   	leave  
 304:	c3                   	ret    
 305:	90                   	nop
 306:	90                   	nop
 307:	90                   	nop

00000308 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 308:	b8 01 00 00 00       	mov    $0x1,%eax
 30d:	cd 40                	int    $0x40
 30f:	c3                   	ret    

00000310 <exit>:
SYSCALL(exit)
 310:	b8 02 00 00 00       	mov    $0x2,%eax
 315:	cd 40                	int    $0x40
 317:	c3                   	ret    

00000318 <wait>:
SYSCALL(wait)
 318:	b8 03 00 00 00       	mov    $0x3,%eax
 31d:	cd 40                	int    $0x40
 31f:	c3                   	ret    

00000320 <pipe>:
SYSCALL(pipe)
 320:	b8 04 00 00 00       	mov    $0x4,%eax
 325:	cd 40                	int    $0x40
 327:	c3                   	ret    

00000328 <read>:
SYSCALL(read)
 328:	b8 05 00 00 00       	mov    $0x5,%eax
 32d:	cd 40                	int    $0x40
 32f:	c3                   	ret    

00000330 <write>:
SYSCALL(write)
 330:	b8 10 00 00 00       	mov    $0x10,%eax
 335:	cd 40                	int    $0x40
 337:	c3                   	ret    

00000338 <close>:
SYSCALL(close)
 338:	b8 15 00 00 00       	mov    $0x15,%eax
 33d:	cd 40                	int    $0x40
 33f:	c3                   	ret    

00000340 <kill>:
SYSCALL(kill)
 340:	b8 06 00 00 00       	mov    $0x6,%eax
 345:	cd 40                	int    $0x40
 347:	c3                   	ret    

00000348 <exec>:
SYSCALL(exec)
 348:	b8 07 00 00 00       	mov    $0x7,%eax
 34d:	cd 40                	int    $0x40
 34f:	c3                   	ret    

00000350 <open>:
SYSCALL(open)
 350:	b8 0f 00 00 00       	mov    $0xf,%eax
 355:	cd 40                	int    $0x40
 357:	c3                   	ret    

00000358 <mknod>:
SYSCALL(mknod)
 358:	b8 11 00 00 00       	mov    $0x11,%eax
 35d:	cd 40                	int    $0x40
 35f:	c3                   	ret    

00000360 <unlink>:
SYSCALL(unlink)
 360:	b8 12 00 00 00       	mov    $0x12,%eax
 365:	cd 40                	int    $0x40
 367:	c3                   	ret    

00000368 <fstat>:
SYSCALL(fstat)
 368:	b8 08 00 00 00       	mov    $0x8,%eax
 36d:	cd 40                	int    $0x40
 36f:	c3                   	ret    

00000370 <link>:
SYSCALL(link)
 370:	b8 13 00 00 00       	mov    $0x13,%eax
 375:	cd 40                	int    $0x40
 377:	c3                   	ret    

00000378 <mkdir>:
SYSCALL(mkdir)
 378:	b8 14 00 00 00       	mov    $0x14,%eax
 37d:	cd 40                	int    $0x40
 37f:	c3                   	ret    

00000380 <chdir>:
SYSCALL(chdir)
 380:	b8 09 00 00 00       	mov    $0x9,%eax
 385:	cd 40                	int    $0x40
 387:	c3                   	ret    

00000388 <dup>:
SYSCALL(dup)
 388:	b8 0a 00 00 00       	mov    $0xa,%eax
 38d:	cd 40                	int    $0x40
 38f:	c3                   	ret    

00000390 <getpid>:
SYSCALL(getpid)
 390:	b8 0b 00 00 00       	mov    $0xb,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <sbrk>:
SYSCALL(sbrk)
 398:	b8 0c 00 00 00       	mov    $0xc,%eax
 39d:	cd 40                	int    $0x40
 39f:	c3                   	ret    

000003a0 <sleep>:
SYSCALL(sleep)
 3a0:	b8 0d 00 00 00       	mov    $0xd,%eax
 3a5:	cd 40                	int    $0x40
 3a7:	c3                   	ret    

000003a8 <uptime>:
SYSCALL(uptime)
 3a8:	b8 0e 00 00 00       	mov    $0xe,%eax
 3ad:	cd 40                	int    $0x40
 3af:	c3                   	ret    

000003b0 <procstats>:
SYSCALL(procstats)
 3b0:	b8 16 00 00 00       	mov    $0x16,%eax
 3b5:	cd 40                	int    $0x40
 3b7:	c3                   	ret    

000003b8 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 3b8:	55                   	push   %ebp
 3b9:	89 e5                	mov    %esp,%ebp
 3bb:	83 ec 28             	sub    $0x28,%esp
 3be:	8b 45 0c             	mov    0xc(%ebp),%eax
 3c1:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 3c4:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 3cb:	00 
 3cc:	8d 45 f4             	lea    -0xc(%ebp),%eax
 3cf:	89 44 24 04          	mov    %eax,0x4(%esp)
 3d3:	8b 45 08             	mov    0x8(%ebp),%eax
 3d6:	89 04 24             	mov    %eax,(%esp)
 3d9:	e8 52 ff ff ff       	call   330 <write>
}
 3de:	c9                   	leave  
 3df:	c3                   	ret    

000003e0 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 3e0:	55                   	push   %ebp
 3e1:	89 e5                	mov    %esp,%ebp
 3e3:	53                   	push   %ebx
 3e4:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 3e7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 3ee:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 3f2:	74 17                	je     40b <printint+0x2b>
 3f4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 3f8:	79 11                	jns    40b <printint+0x2b>
    neg = 1;
 3fa:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 401:	8b 45 0c             	mov    0xc(%ebp),%eax
 404:	f7 d8                	neg    %eax
 406:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 409:	eb 06                	jmp    411 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 40b:	8b 45 0c             	mov    0xc(%ebp),%eax
 40e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 411:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 418:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 41b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 41e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 421:	ba 00 00 00 00       	mov    $0x0,%edx
 426:	f7 f3                	div    %ebx
 428:	89 d0                	mov    %edx,%eax
 42a:	0f b6 80 78 08 00 00 	movzbl 0x878(%eax),%eax
 431:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 435:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 439:	8b 45 10             	mov    0x10(%ebp),%eax
 43c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 43f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 442:	ba 00 00 00 00       	mov    $0x0,%edx
 447:	f7 75 d4             	divl   -0x2c(%ebp)
 44a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 44d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 451:	75 c5                	jne    418 <printint+0x38>
  if(neg)
 453:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 457:	74 2a                	je     483 <printint+0xa3>
    buf[i++] = '-';
 459:	8b 45 ec             	mov    -0x14(%ebp),%eax
 45c:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 461:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 465:	eb 1d                	jmp    484 <printint+0xa4>
    putc(fd, buf[i]);
 467:	8b 45 ec             	mov    -0x14(%ebp),%eax
 46a:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 46f:	0f be c0             	movsbl %al,%eax
 472:	89 44 24 04          	mov    %eax,0x4(%esp)
 476:	8b 45 08             	mov    0x8(%ebp),%eax
 479:	89 04 24             	mov    %eax,(%esp)
 47c:	e8 37 ff ff ff       	call   3b8 <putc>
 481:	eb 01                	jmp    484 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 483:	90                   	nop
 484:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 488:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 48c:	79 d9                	jns    467 <printint+0x87>
    putc(fd, buf[i]);
}
 48e:	83 c4 44             	add    $0x44,%esp
 491:	5b                   	pop    %ebx
 492:	5d                   	pop    %ebp
 493:	c3                   	ret    

00000494 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 494:	55                   	push   %ebp
 495:	89 e5                	mov    %esp,%ebp
 497:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 49a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 4a1:	8d 45 0c             	lea    0xc(%ebp),%eax
 4a4:	83 c0 04             	add    $0x4,%eax
 4a7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 4aa:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 4b1:	e9 7e 01 00 00       	jmp    634 <printf+0x1a0>
    c = fmt[i] & 0xff;
 4b6:	8b 55 0c             	mov    0xc(%ebp),%edx
 4b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4bc:	8d 04 02             	lea    (%edx,%eax,1),%eax
 4bf:	0f b6 00             	movzbl (%eax),%eax
 4c2:	0f be c0             	movsbl %al,%eax
 4c5:	25 ff 00 00 00       	and    $0xff,%eax
 4ca:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 4cd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4d1:	75 2c                	jne    4ff <printf+0x6b>
      if(c == '%'){
 4d3:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 4d7:	75 0c                	jne    4e5 <printf+0x51>
        state = '%';
 4d9:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 4e0:	e9 4b 01 00 00       	jmp    630 <printf+0x19c>
      } else {
        putc(fd, c);
 4e5:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4e8:	0f be c0             	movsbl %al,%eax
 4eb:	89 44 24 04          	mov    %eax,0x4(%esp)
 4ef:	8b 45 08             	mov    0x8(%ebp),%eax
 4f2:	89 04 24             	mov    %eax,(%esp)
 4f5:	e8 be fe ff ff       	call   3b8 <putc>
 4fa:	e9 31 01 00 00       	jmp    630 <printf+0x19c>
      }
    } else if(state == '%'){
 4ff:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 503:	0f 85 27 01 00 00    	jne    630 <printf+0x19c>
      if(c == 'd'){
 509:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 50d:	75 2d                	jne    53c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 50f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 512:	8b 00                	mov    (%eax),%eax
 514:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 51b:	00 
 51c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 523:	00 
 524:	89 44 24 04          	mov    %eax,0x4(%esp)
 528:	8b 45 08             	mov    0x8(%ebp),%eax
 52b:	89 04 24             	mov    %eax,(%esp)
 52e:	e8 ad fe ff ff       	call   3e0 <printint>
        ap++;
 533:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 537:	e9 ed 00 00 00       	jmp    629 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 53c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 540:	74 06                	je     548 <printf+0xb4>
 542:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 546:	75 2d                	jne    575 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 548:	8b 45 f4             	mov    -0xc(%ebp),%eax
 54b:	8b 00                	mov    (%eax),%eax
 54d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 554:	00 
 555:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 55c:	00 
 55d:	89 44 24 04          	mov    %eax,0x4(%esp)
 561:	8b 45 08             	mov    0x8(%ebp),%eax
 564:	89 04 24             	mov    %eax,(%esp)
 567:	e8 74 fe ff ff       	call   3e0 <printint>
        ap++;
 56c:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 570:	e9 b4 00 00 00       	jmp    629 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 575:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 579:	75 46                	jne    5c1 <printf+0x12d>
        s = (char*)*ap;
 57b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 57e:	8b 00                	mov    (%eax),%eax
 580:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 583:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 587:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 58b:	75 27                	jne    5b4 <printf+0x120>
          s = "(null)";
 58d:	c7 45 e4 6f 08 00 00 	movl   $0x86f,-0x1c(%ebp)
        while(*s != 0){
 594:	eb 1f                	jmp    5b5 <printf+0x121>
          putc(fd, *s);
 596:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 599:	0f b6 00             	movzbl (%eax),%eax
 59c:	0f be c0             	movsbl %al,%eax
 59f:	89 44 24 04          	mov    %eax,0x4(%esp)
 5a3:	8b 45 08             	mov    0x8(%ebp),%eax
 5a6:	89 04 24             	mov    %eax,(%esp)
 5a9:	e8 0a fe ff ff       	call   3b8 <putc>
          s++;
 5ae:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 5b2:	eb 01                	jmp    5b5 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 5b4:	90                   	nop
 5b5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5b8:	0f b6 00             	movzbl (%eax),%eax
 5bb:	84 c0                	test   %al,%al
 5bd:	75 d7                	jne    596 <printf+0x102>
 5bf:	eb 68                	jmp    629 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5c1:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 5c5:	75 1d                	jne    5e4 <printf+0x150>
        putc(fd, *ap);
 5c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5ca:	8b 00                	mov    (%eax),%eax
 5cc:	0f be c0             	movsbl %al,%eax
 5cf:	89 44 24 04          	mov    %eax,0x4(%esp)
 5d3:	8b 45 08             	mov    0x8(%ebp),%eax
 5d6:	89 04 24             	mov    %eax,(%esp)
 5d9:	e8 da fd ff ff       	call   3b8 <putc>
        ap++;
 5de:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5e2:	eb 45                	jmp    629 <printf+0x195>
      } else if(c == '%'){
 5e4:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 5e8:	75 17                	jne    601 <printf+0x16d>
        putc(fd, c);
 5ea:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5ed:	0f be c0             	movsbl %al,%eax
 5f0:	89 44 24 04          	mov    %eax,0x4(%esp)
 5f4:	8b 45 08             	mov    0x8(%ebp),%eax
 5f7:	89 04 24             	mov    %eax,(%esp)
 5fa:	e8 b9 fd ff ff       	call   3b8 <putc>
 5ff:	eb 28                	jmp    629 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 601:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 608:	00 
 609:	8b 45 08             	mov    0x8(%ebp),%eax
 60c:	89 04 24             	mov    %eax,(%esp)
 60f:	e8 a4 fd ff ff       	call   3b8 <putc>
        putc(fd, c);
 614:	8b 45 e8             	mov    -0x18(%ebp),%eax
 617:	0f be c0             	movsbl %al,%eax
 61a:	89 44 24 04          	mov    %eax,0x4(%esp)
 61e:	8b 45 08             	mov    0x8(%ebp),%eax
 621:	89 04 24             	mov    %eax,(%esp)
 624:	e8 8f fd ff ff       	call   3b8 <putc>
      }
      state = 0;
 629:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 630:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 634:	8b 55 0c             	mov    0xc(%ebp),%edx
 637:	8b 45 ec             	mov    -0x14(%ebp),%eax
 63a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 63d:	0f b6 00             	movzbl (%eax),%eax
 640:	84 c0                	test   %al,%al
 642:	0f 85 6e fe ff ff    	jne    4b6 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 648:	c9                   	leave  
 649:	c3                   	ret    
 64a:	90                   	nop
 64b:	90                   	nop

0000064c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 64c:	55                   	push   %ebp
 64d:	89 e5                	mov    %esp,%ebp
 64f:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 652:	8b 45 08             	mov    0x8(%ebp),%eax
 655:	83 e8 08             	sub    $0x8,%eax
 658:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 65b:	a1 94 08 00 00       	mov    0x894,%eax
 660:	89 45 fc             	mov    %eax,-0x4(%ebp)
 663:	eb 24                	jmp    689 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 665:	8b 45 fc             	mov    -0x4(%ebp),%eax
 668:	8b 00                	mov    (%eax),%eax
 66a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 66d:	77 12                	ja     681 <free+0x35>
 66f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 672:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 675:	77 24                	ja     69b <free+0x4f>
 677:	8b 45 fc             	mov    -0x4(%ebp),%eax
 67a:	8b 00                	mov    (%eax),%eax
 67c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 67f:	77 1a                	ja     69b <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 681:	8b 45 fc             	mov    -0x4(%ebp),%eax
 684:	8b 00                	mov    (%eax),%eax
 686:	89 45 fc             	mov    %eax,-0x4(%ebp)
 689:	8b 45 f8             	mov    -0x8(%ebp),%eax
 68c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 68f:	76 d4                	jbe    665 <free+0x19>
 691:	8b 45 fc             	mov    -0x4(%ebp),%eax
 694:	8b 00                	mov    (%eax),%eax
 696:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 699:	76 ca                	jbe    665 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 69b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 69e:	8b 40 04             	mov    0x4(%eax),%eax
 6a1:	c1 e0 03             	shl    $0x3,%eax
 6a4:	89 c2                	mov    %eax,%edx
 6a6:	03 55 f8             	add    -0x8(%ebp),%edx
 6a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ac:	8b 00                	mov    (%eax),%eax
 6ae:	39 c2                	cmp    %eax,%edx
 6b0:	75 24                	jne    6d6 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 6b2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6b5:	8b 50 04             	mov    0x4(%eax),%edx
 6b8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6bb:	8b 00                	mov    (%eax),%eax
 6bd:	8b 40 04             	mov    0x4(%eax),%eax
 6c0:	01 c2                	add    %eax,%edx
 6c2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6c5:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6c8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6cb:	8b 00                	mov    (%eax),%eax
 6cd:	8b 10                	mov    (%eax),%edx
 6cf:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d2:	89 10                	mov    %edx,(%eax)
 6d4:	eb 0a                	jmp    6e0 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 6d6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d9:	8b 10                	mov    (%eax),%edx
 6db:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6de:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6e0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e3:	8b 40 04             	mov    0x4(%eax),%eax
 6e6:	c1 e0 03             	shl    $0x3,%eax
 6e9:	03 45 fc             	add    -0x4(%ebp),%eax
 6ec:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6ef:	75 20                	jne    711 <free+0xc5>
    p->s.size += bp->s.size;
 6f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f4:	8b 50 04             	mov    0x4(%eax),%edx
 6f7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fa:	8b 40 04             	mov    0x4(%eax),%eax
 6fd:	01 c2                	add    %eax,%edx
 6ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
 702:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 705:	8b 45 f8             	mov    -0x8(%ebp),%eax
 708:	8b 10                	mov    (%eax),%edx
 70a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 70d:	89 10                	mov    %edx,(%eax)
 70f:	eb 08                	jmp    719 <free+0xcd>
  } else
    p->s.ptr = bp;
 711:	8b 45 fc             	mov    -0x4(%ebp),%eax
 714:	8b 55 f8             	mov    -0x8(%ebp),%edx
 717:	89 10                	mov    %edx,(%eax)
  freep = p;
 719:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71c:	a3 94 08 00 00       	mov    %eax,0x894
}
 721:	c9                   	leave  
 722:	c3                   	ret    

00000723 <morecore>:

static Header*
morecore(uint nu)
{
 723:	55                   	push   %ebp
 724:	89 e5                	mov    %esp,%ebp
 726:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 729:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 730:	77 07                	ja     739 <morecore+0x16>
    nu = 4096;
 732:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 739:	8b 45 08             	mov    0x8(%ebp),%eax
 73c:	c1 e0 03             	shl    $0x3,%eax
 73f:	89 04 24             	mov    %eax,(%esp)
 742:	e8 51 fc ff ff       	call   398 <sbrk>
 747:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 74a:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 74e:	75 07                	jne    757 <morecore+0x34>
    return 0;
 750:	b8 00 00 00 00       	mov    $0x0,%eax
 755:	eb 22                	jmp    779 <morecore+0x56>
  hp = (Header*)p;
 757:	8b 45 f0             	mov    -0x10(%ebp),%eax
 75a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 75d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 760:	8b 55 08             	mov    0x8(%ebp),%edx
 763:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 766:	8b 45 f4             	mov    -0xc(%ebp),%eax
 769:	83 c0 08             	add    $0x8,%eax
 76c:	89 04 24             	mov    %eax,(%esp)
 76f:	e8 d8 fe ff ff       	call   64c <free>
  return freep;
 774:	a1 94 08 00 00       	mov    0x894,%eax
}
 779:	c9                   	leave  
 77a:	c3                   	ret    

0000077b <malloc>:

void*
malloc(uint nbytes)
{
 77b:	55                   	push   %ebp
 77c:	89 e5                	mov    %esp,%ebp
 77e:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 781:	8b 45 08             	mov    0x8(%ebp),%eax
 784:	83 c0 07             	add    $0x7,%eax
 787:	c1 e8 03             	shr    $0x3,%eax
 78a:	83 c0 01             	add    $0x1,%eax
 78d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 790:	a1 94 08 00 00       	mov    0x894,%eax
 795:	89 45 f0             	mov    %eax,-0x10(%ebp)
 798:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 79c:	75 23                	jne    7c1 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 79e:	c7 45 f0 8c 08 00 00 	movl   $0x88c,-0x10(%ebp)
 7a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7a8:	a3 94 08 00 00       	mov    %eax,0x894
 7ad:	a1 94 08 00 00       	mov    0x894,%eax
 7b2:	a3 8c 08 00 00       	mov    %eax,0x88c
    base.s.size = 0;
 7b7:	c7 05 90 08 00 00 00 	movl   $0x0,0x890
 7be:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7c4:	8b 00                	mov    (%eax),%eax
 7c6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 7c9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7cc:	8b 40 04             	mov    0x4(%eax),%eax
 7cf:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7d2:	72 4d                	jb     821 <malloc+0xa6>
      if(p->s.size == nunits)
 7d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7d7:	8b 40 04             	mov    0x4(%eax),%eax
 7da:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7dd:	75 0c                	jne    7eb <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 7df:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7e2:	8b 10                	mov    (%eax),%edx
 7e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7e7:	89 10                	mov    %edx,(%eax)
 7e9:	eb 26                	jmp    811 <malloc+0x96>
      else {
        p->s.size -= nunits;
 7eb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ee:	8b 40 04             	mov    0x4(%eax),%eax
 7f1:	89 c2                	mov    %eax,%edx
 7f3:	2b 55 f4             	sub    -0xc(%ebp),%edx
 7f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7f9:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 7fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ff:	8b 40 04             	mov    0x4(%eax),%eax
 802:	c1 e0 03             	shl    $0x3,%eax
 805:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 808:	8b 45 ec             	mov    -0x14(%ebp),%eax
 80b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 80e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 811:	8b 45 f0             	mov    -0x10(%ebp),%eax
 814:	a3 94 08 00 00       	mov    %eax,0x894
      return (void*)(p + 1);
 819:	8b 45 ec             	mov    -0x14(%ebp),%eax
 81c:	83 c0 08             	add    $0x8,%eax
 81f:	eb 38                	jmp    859 <malloc+0xde>
    }
    if(p == freep)
 821:	a1 94 08 00 00       	mov    0x894,%eax
 826:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 829:	75 1b                	jne    846 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 82b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 82e:	89 04 24             	mov    %eax,(%esp)
 831:	e8 ed fe ff ff       	call   723 <morecore>
 836:	89 45 ec             	mov    %eax,-0x14(%ebp)
 839:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 83d:	75 07                	jne    846 <malloc+0xcb>
        return 0;
 83f:	b8 00 00 00 00       	mov    $0x0,%eax
 844:	eb 13                	jmp    859 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 846:	8b 45 ec             	mov    -0x14(%ebp),%eax
 849:	89 45 f0             	mov    %eax,-0x10(%ebp)
 84c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 84f:	8b 00                	mov    (%eax),%eax
 851:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 854:	e9 70 ff ff ff       	jmp    7c9 <malloc+0x4e>
}
 859:	c9                   	leave  
 85a:	c3                   	ret    
