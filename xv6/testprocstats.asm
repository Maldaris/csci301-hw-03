
_testprocstats:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	53                   	push   %ebx
   7:	83 ec 4c             	sub    $0x4c,%esp
    int running, runnable, sleeping, zombie;
    procstats(&running, &runnable, &sleeping, &zombie);
   a:	8d 44 24 2c          	lea    0x2c(%esp),%eax
   e:	89 44 24 0c          	mov    %eax,0xc(%esp)
  12:	8d 44 24 30          	lea    0x30(%esp),%eax
  16:	89 44 24 08          	mov    %eax,0x8(%esp)
  1a:	8d 44 24 34          	lea    0x34(%esp),%eax
  1e:	89 44 24 04          	mov    %eax,0x4(%esp)
  22:	8d 44 24 38          	lea    0x38(%esp),%eax
  26:	89 04 24             	mov    %eax,(%esp)
  29:	e8 ea 03 00 00       	call   418 <procstats>
    printf(1, "Running: %d, runnable: %d, sleeping: %d, zombie: %d\n", running, runnable, sleeping, zombie);
  2e:	8b 5c 24 2c          	mov    0x2c(%esp),%ebx
  32:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  36:	8b 54 24 34          	mov    0x34(%esp),%edx
  3a:	8b 44 24 38          	mov    0x38(%esp),%eax
  3e:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  42:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  46:	89 54 24 0c          	mov    %edx,0xc(%esp)
  4a:	89 44 24 08          	mov    %eax,0x8(%esp)
  4e:	c7 44 24 04 c4 08 00 	movl   $0x8c4,0x4(%esp)
  55:	00 
  56:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  5d:	e8 9a 04 00 00       	call   4fc <printf>
    int pid = fork();
  62:	e8 09 03 00 00       	call   370 <fork>
  67:	89 44 24 3c          	mov    %eax,0x3c(%esp)
    if(pid == 0){
  6b:	83 7c 24 3c 00       	cmpl   $0x0,0x3c(%esp)
  70:	75 11                	jne    83 <main+0x83>
      sleep(100);
  72:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
  79:	e8 8a 03 00 00       	call   408 <sleep>
  7e:	e9 8c 00 00 00       	jmp    10f <main+0x10f>
    } else {
      kill(pid);
  83:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  87:	89 04 24             	mov    %eax,(%esp)
  8a:	e8 19 03 00 00       	call   3a8 <kill>
      sleep(10);
  8f:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  96:	e8 6d 03 00 00       	call   408 <sleep>
      printf(1, "%s", "Creating zombie child...\n");
  9b:	c7 44 24 08 f9 08 00 	movl   $0x8f9,0x8(%esp)
  a2:	00 
  a3:	c7 44 24 04 13 09 00 	movl   $0x913,0x4(%esp)
  aa:	00 
  ab:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  b2:	e8 45 04 00 00       	call   4fc <printf>
      procstats(&running, &runnable, &sleeping, &zombie);
  b7:	8d 44 24 2c          	lea    0x2c(%esp),%eax
  bb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  bf:	8d 44 24 30          	lea    0x30(%esp),%eax
  c3:	89 44 24 08          	mov    %eax,0x8(%esp)
  c7:	8d 44 24 34          	lea    0x34(%esp),%eax
  cb:	89 44 24 04          	mov    %eax,0x4(%esp)
  cf:	8d 44 24 38          	lea    0x38(%esp),%eax
  d3:	89 04 24             	mov    %eax,(%esp)
  d6:	e8 3d 03 00 00       	call   418 <procstats>
      printf(1, "Running: %d, runnable: %d, sleeping: %d, zombie: %d\n", running, runnable, sleeping, zombie);
  db:	8b 5c 24 2c          	mov    0x2c(%esp),%ebx
  df:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  e3:	8b 54 24 34          	mov    0x34(%esp),%edx
  e7:	8b 44 24 38          	mov    0x38(%esp),%eax
  eb:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  ef:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  f3:	89 54 24 0c          	mov    %edx,0xc(%esp)
  f7:	89 44 24 08          	mov    %eax,0x8(%esp)
  fb:	c7 44 24 04 c4 08 00 	movl   $0x8c4,0x4(%esp)
 102:	00 
 103:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 10a:	e8 ed 03 00 00       	call   4fc <printf>

    }
  exit();
 10f:	e8 64 02 00 00       	call   378 <exit>

00000114 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 114:	55                   	push   %ebp
 115:	89 e5                	mov    %esp,%ebp
 117:	57                   	push   %edi
 118:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 119:	8b 4d 08             	mov    0x8(%ebp),%ecx
 11c:	8b 55 10             	mov    0x10(%ebp),%edx
 11f:	8b 45 0c             	mov    0xc(%ebp),%eax
 122:	89 cb                	mov    %ecx,%ebx
 124:	89 df                	mov    %ebx,%edi
 126:	89 d1                	mov    %edx,%ecx
 128:	fc                   	cld    
 129:	f3 aa                	rep stos %al,%es:(%edi)
 12b:	89 ca                	mov    %ecx,%edx
 12d:	89 fb                	mov    %edi,%ebx
 12f:	89 5d 08             	mov    %ebx,0x8(%ebp)
 132:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 135:	5b                   	pop    %ebx
 136:	5f                   	pop    %edi
 137:	5d                   	pop    %ebp
 138:	c3                   	ret    

00000139 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 139:	55                   	push   %ebp
 13a:	89 e5                	mov    %esp,%ebp
 13c:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 13f:	8b 45 08             	mov    0x8(%ebp),%eax
 142:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 145:	8b 45 0c             	mov    0xc(%ebp),%eax
 148:	0f b6 10             	movzbl (%eax),%edx
 14b:	8b 45 08             	mov    0x8(%ebp),%eax
 14e:	88 10                	mov    %dl,(%eax)
 150:	8b 45 08             	mov    0x8(%ebp),%eax
 153:	0f b6 00             	movzbl (%eax),%eax
 156:	84 c0                	test   %al,%al
 158:	0f 95 c0             	setne  %al
 15b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 15f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 163:	84 c0                	test   %al,%al
 165:	75 de                	jne    145 <strcpy+0xc>
    ;
  return os;
 167:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 16a:	c9                   	leave  
 16b:	c3                   	ret    

0000016c <strcmp>:

int
strcmp(const char *p, const char *q)
{
 16c:	55                   	push   %ebp
 16d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 16f:	eb 08                	jmp    179 <strcmp+0xd>
    p++, q++;
 171:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 175:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 179:	8b 45 08             	mov    0x8(%ebp),%eax
 17c:	0f b6 00             	movzbl (%eax),%eax
 17f:	84 c0                	test   %al,%al
 181:	74 10                	je     193 <strcmp+0x27>
 183:	8b 45 08             	mov    0x8(%ebp),%eax
 186:	0f b6 10             	movzbl (%eax),%edx
 189:	8b 45 0c             	mov    0xc(%ebp),%eax
 18c:	0f b6 00             	movzbl (%eax),%eax
 18f:	38 c2                	cmp    %al,%dl
 191:	74 de                	je     171 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 193:	8b 45 08             	mov    0x8(%ebp),%eax
 196:	0f b6 00             	movzbl (%eax),%eax
 199:	0f b6 d0             	movzbl %al,%edx
 19c:	8b 45 0c             	mov    0xc(%ebp),%eax
 19f:	0f b6 00             	movzbl (%eax),%eax
 1a2:	0f b6 c0             	movzbl %al,%eax
 1a5:	89 d1                	mov    %edx,%ecx
 1a7:	29 c1                	sub    %eax,%ecx
 1a9:	89 c8                	mov    %ecx,%eax
}
 1ab:	5d                   	pop    %ebp
 1ac:	c3                   	ret    

000001ad <strlen>:

uint
strlen(char *s)
{
 1ad:	55                   	push   %ebp
 1ae:	89 e5                	mov    %esp,%ebp
 1b0:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1b3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1ba:	eb 04                	jmp    1c0 <strlen+0x13>
 1bc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1c3:	03 45 08             	add    0x8(%ebp),%eax
 1c6:	0f b6 00             	movzbl (%eax),%eax
 1c9:	84 c0                	test   %al,%al
 1cb:	75 ef                	jne    1bc <strlen+0xf>
    ;
  return n;
 1cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1d0:	c9                   	leave  
 1d1:	c3                   	ret    

000001d2 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1d2:	55                   	push   %ebp
 1d3:	89 e5                	mov    %esp,%ebp
 1d5:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 1d8:	8b 45 10             	mov    0x10(%ebp),%eax
 1db:	89 44 24 08          	mov    %eax,0x8(%esp)
 1df:	8b 45 0c             	mov    0xc(%ebp),%eax
 1e2:	89 44 24 04          	mov    %eax,0x4(%esp)
 1e6:	8b 45 08             	mov    0x8(%ebp),%eax
 1e9:	89 04 24             	mov    %eax,(%esp)
 1ec:	e8 23 ff ff ff       	call   114 <stosb>
  return dst;
 1f1:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1f4:	c9                   	leave  
 1f5:	c3                   	ret    

000001f6 <strchr>:

char*
strchr(const char *s, char c)
{
 1f6:	55                   	push   %ebp
 1f7:	89 e5                	mov    %esp,%ebp
 1f9:	83 ec 04             	sub    $0x4,%esp
 1fc:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ff:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 202:	eb 14                	jmp    218 <strchr+0x22>
    if(*s == c)
 204:	8b 45 08             	mov    0x8(%ebp),%eax
 207:	0f b6 00             	movzbl (%eax),%eax
 20a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 20d:	75 05                	jne    214 <strchr+0x1e>
      return (char*)s;
 20f:	8b 45 08             	mov    0x8(%ebp),%eax
 212:	eb 13                	jmp    227 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 214:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 218:	8b 45 08             	mov    0x8(%ebp),%eax
 21b:	0f b6 00             	movzbl (%eax),%eax
 21e:	84 c0                	test   %al,%al
 220:	75 e2                	jne    204 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 222:	b8 00 00 00 00       	mov    $0x0,%eax
}
 227:	c9                   	leave  
 228:	c3                   	ret    

00000229 <gets>:

char*
gets(char *buf, int max)
{
 229:	55                   	push   %ebp
 22a:	89 e5                	mov    %esp,%ebp
 22c:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 22f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 236:	eb 44                	jmp    27c <gets+0x53>
    cc = read(0, &c, 1);
 238:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 23f:	00 
 240:	8d 45 ef             	lea    -0x11(%ebp),%eax
 243:	89 44 24 04          	mov    %eax,0x4(%esp)
 247:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 24e:	e8 3d 01 00 00       	call   390 <read>
 253:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 256:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 25a:	7e 2d                	jle    289 <gets+0x60>
      break;
    buf[i++] = c;
 25c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 25f:	03 45 08             	add    0x8(%ebp),%eax
 262:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 266:	88 10                	mov    %dl,(%eax)
 268:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 26c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 270:	3c 0a                	cmp    $0xa,%al
 272:	74 16                	je     28a <gets+0x61>
 274:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 278:	3c 0d                	cmp    $0xd,%al
 27a:	74 0e                	je     28a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 27c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 27f:	83 c0 01             	add    $0x1,%eax
 282:	3b 45 0c             	cmp    0xc(%ebp),%eax
 285:	7c b1                	jl     238 <gets+0xf>
 287:	eb 01                	jmp    28a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 289:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 28a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 28d:	03 45 08             	add    0x8(%ebp),%eax
 290:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 293:	8b 45 08             	mov    0x8(%ebp),%eax
}
 296:	c9                   	leave  
 297:	c3                   	ret    

00000298 <stat>:

int
stat(char *n, struct stat *st)
{
 298:	55                   	push   %ebp
 299:	89 e5                	mov    %esp,%ebp
 29b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 29e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 2a5:	00 
 2a6:	8b 45 08             	mov    0x8(%ebp),%eax
 2a9:	89 04 24             	mov    %eax,(%esp)
 2ac:	e8 07 01 00 00       	call   3b8 <open>
 2b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 2b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 2b8:	79 07                	jns    2c1 <stat+0x29>
    return -1;
 2ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2bf:	eb 23                	jmp    2e4 <stat+0x4c>
  r = fstat(fd, st);
 2c1:	8b 45 0c             	mov    0xc(%ebp),%eax
 2c4:	89 44 24 04          	mov    %eax,0x4(%esp)
 2c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2cb:	89 04 24             	mov    %eax,(%esp)
 2ce:	e8 fd 00 00 00       	call   3d0 <fstat>
 2d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 2d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2d9:	89 04 24             	mov    %eax,(%esp)
 2dc:	e8 bf 00 00 00       	call   3a0 <close>
  return r;
 2e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 2e4:	c9                   	leave  
 2e5:	c3                   	ret    

000002e6 <atoi>:

int
atoi(const char *s)
{
 2e6:	55                   	push   %ebp
 2e7:	89 e5                	mov    %esp,%ebp
 2e9:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 2ec:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 2f3:	eb 24                	jmp    319 <atoi+0x33>
    n = n*10 + *s++ - '0';
 2f5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 2f8:	89 d0                	mov    %edx,%eax
 2fa:	c1 e0 02             	shl    $0x2,%eax
 2fd:	01 d0                	add    %edx,%eax
 2ff:	01 c0                	add    %eax,%eax
 301:	89 c2                	mov    %eax,%edx
 303:	8b 45 08             	mov    0x8(%ebp),%eax
 306:	0f b6 00             	movzbl (%eax),%eax
 309:	0f be c0             	movsbl %al,%eax
 30c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 30f:	83 e8 30             	sub    $0x30,%eax
 312:	89 45 fc             	mov    %eax,-0x4(%ebp)
 315:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 319:	8b 45 08             	mov    0x8(%ebp),%eax
 31c:	0f b6 00             	movzbl (%eax),%eax
 31f:	3c 2f                	cmp    $0x2f,%al
 321:	7e 0a                	jle    32d <atoi+0x47>
 323:	8b 45 08             	mov    0x8(%ebp),%eax
 326:	0f b6 00             	movzbl (%eax),%eax
 329:	3c 39                	cmp    $0x39,%al
 32b:	7e c8                	jle    2f5 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 32d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 330:	c9                   	leave  
 331:	c3                   	ret    

00000332 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 332:	55                   	push   %ebp
 333:	89 e5                	mov    %esp,%ebp
 335:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 338:	8b 45 08             	mov    0x8(%ebp),%eax
 33b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 33e:	8b 45 0c             	mov    0xc(%ebp),%eax
 341:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 344:	eb 13                	jmp    359 <memmove+0x27>
    *dst++ = *src++;
 346:	8b 45 fc             	mov    -0x4(%ebp),%eax
 349:	0f b6 10             	movzbl (%eax),%edx
 34c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 34f:	88 10                	mov    %dl,(%eax)
 351:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 355:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 359:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 35d:	0f 9f c0             	setg   %al
 360:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 364:	84 c0                	test   %al,%al
 366:	75 de                	jne    346 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 368:	8b 45 08             	mov    0x8(%ebp),%eax
}
 36b:	c9                   	leave  
 36c:	c3                   	ret    
 36d:	90                   	nop
 36e:	90                   	nop
 36f:	90                   	nop

00000370 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 370:	b8 01 00 00 00       	mov    $0x1,%eax
 375:	cd 40                	int    $0x40
 377:	c3                   	ret    

00000378 <exit>:
SYSCALL(exit)
 378:	b8 02 00 00 00       	mov    $0x2,%eax
 37d:	cd 40                	int    $0x40
 37f:	c3                   	ret    

00000380 <wait>:
SYSCALL(wait)
 380:	b8 03 00 00 00       	mov    $0x3,%eax
 385:	cd 40                	int    $0x40
 387:	c3                   	ret    

00000388 <pipe>:
SYSCALL(pipe)
 388:	b8 04 00 00 00       	mov    $0x4,%eax
 38d:	cd 40                	int    $0x40
 38f:	c3                   	ret    

00000390 <read>:
SYSCALL(read)
 390:	b8 05 00 00 00       	mov    $0x5,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <write>:
SYSCALL(write)
 398:	b8 10 00 00 00       	mov    $0x10,%eax
 39d:	cd 40                	int    $0x40
 39f:	c3                   	ret    

000003a0 <close>:
SYSCALL(close)
 3a0:	b8 15 00 00 00       	mov    $0x15,%eax
 3a5:	cd 40                	int    $0x40
 3a7:	c3                   	ret    

000003a8 <kill>:
SYSCALL(kill)
 3a8:	b8 06 00 00 00       	mov    $0x6,%eax
 3ad:	cd 40                	int    $0x40
 3af:	c3                   	ret    

000003b0 <exec>:
SYSCALL(exec)
 3b0:	b8 07 00 00 00       	mov    $0x7,%eax
 3b5:	cd 40                	int    $0x40
 3b7:	c3                   	ret    

000003b8 <open>:
SYSCALL(open)
 3b8:	b8 0f 00 00 00       	mov    $0xf,%eax
 3bd:	cd 40                	int    $0x40
 3bf:	c3                   	ret    

000003c0 <mknod>:
SYSCALL(mknod)
 3c0:	b8 11 00 00 00       	mov    $0x11,%eax
 3c5:	cd 40                	int    $0x40
 3c7:	c3                   	ret    

000003c8 <unlink>:
SYSCALL(unlink)
 3c8:	b8 12 00 00 00       	mov    $0x12,%eax
 3cd:	cd 40                	int    $0x40
 3cf:	c3                   	ret    

000003d0 <fstat>:
SYSCALL(fstat)
 3d0:	b8 08 00 00 00       	mov    $0x8,%eax
 3d5:	cd 40                	int    $0x40
 3d7:	c3                   	ret    

000003d8 <link>:
SYSCALL(link)
 3d8:	b8 13 00 00 00       	mov    $0x13,%eax
 3dd:	cd 40                	int    $0x40
 3df:	c3                   	ret    

000003e0 <mkdir>:
SYSCALL(mkdir)
 3e0:	b8 14 00 00 00       	mov    $0x14,%eax
 3e5:	cd 40                	int    $0x40
 3e7:	c3                   	ret    

000003e8 <chdir>:
SYSCALL(chdir)
 3e8:	b8 09 00 00 00       	mov    $0x9,%eax
 3ed:	cd 40                	int    $0x40
 3ef:	c3                   	ret    

000003f0 <dup>:
SYSCALL(dup)
 3f0:	b8 0a 00 00 00       	mov    $0xa,%eax
 3f5:	cd 40                	int    $0x40
 3f7:	c3                   	ret    

000003f8 <getpid>:
SYSCALL(getpid)
 3f8:	b8 0b 00 00 00       	mov    $0xb,%eax
 3fd:	cd 40                	int    $0x40
 3ff:	c3                   	ret    

00000400 <sbrk>:
SYSCALL(sbrk)
 400:	b8 0c 00 00 00       	mov    $0xc,%eax
 405:	cd 40                	int    $0x40
 407:	c3                   	ret    

00000408 <sleep>:
SYSCALL(sleep)
 408:	b8 0d 00 00 00       	mov    $0xd,%eax
 40d:	cd 40                	int    $0x40
 40f:	c3                   	ret    

00000410 <uptime>:
SYSCALL(uptime)
 410:	b8 0e 00 00 00       	mov    $0xe,%eax
 415:	cd 40                	int    $0x40
 417:	c3                   	ret    

00000418 <procstats>:
SYSCALL(procstats)
 418:	b8 16 00 00 00       	mov    $0x16,%eax
 41d:	cd 40                	int    $0x40
 41f:	c3                   	ret    

00000420 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 420:	55                   	push   %ebp
 421:	89 e5                	mov    %esp,%ebp
 423:	83 ec 28             	sub    $0x28,%esp
 426:	8b 45 0c             	mov    0xc(%ebp),%eax
 429:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 42c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 433:	00 
 434:	8d 45 f4             	lea    -0xc(%ebp),%eax
 437:	89 44 24 04          	mov    %eax,0x4(%esp)
 43b:	8b 45 08             	mov    0x8(%ebp),%eax
 43e:	89 04 24             	mov    %eax,(%esp)
 441:	e8 52 ff ff ff       	call   398 <write>
}
 446:	c9                   	leave  
 447:	c3                   	ret    

00000448 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 448:	55                   	push   %ebp
 449:	89 e5                	mov    %esp,%ebp
 44b:	53                   	push   %ebx
 44c:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 44f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 456:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 45a:	74 17                	je     473 <printint+0x2b>
 45c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 460:	79 11                	jns    473 <printint+0x2b>
    neg = 1;
 462:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 469:	8b 45 0c             	mov    0xc(%ebp),%eax
 46c:	f7 d8                	neg    %eax
 46e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 471:	eb 06                	jmp    479 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 473:	8b 45 0c             	mov    0xc(%ebp),%eax
 476:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 479:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 480:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 483:	8b 5d 10             	mov    0x10(%ebp),%ebx
 486:	8b 45 f4             	mov    -0xc(%ebp),%eax
 489:	ba 00 00 00 00       	mov    $0x0,%edx
 48e:	f7 f3                	div    %ebx
 490:	89 d0                	mov    %edx,%eax
 492:	0f b6 80 20 09 00 00 	movzbl 0x920(%eax),%eax
 499:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 49d:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 4a1:	8b 45 10             	mov    0x10(%ebp),%eax
 4a4:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 4a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4aa:	ba 00 00 00 00       	mov    $0x0,%edx
 4af:	f7 75 d4             	divl   -0x2c(%ebp)
 4b2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4b5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 4b9:	75 c5                	jne    480 <printint+0x38>
  if(neg)
 4bb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4bf:	74 2a                	je     4eb <printint+0xa3>
    buf[i++] = '-';
 4c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4c4:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 4c9:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 4cd:	eb 1d                	jmp    4ec <printint+0xa4>
    putc(fd, buf[i]);
 4cf:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4d2:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 4d7:	0f be c0             	movsbl %al,%eax
 4da:	89 44 24 04          	mov    %eax,0x4(%esp)
 4de:	8b 45 08             	mov    0x8(%ebp),%eax
 4e1:	89 04 24             	mov    %eax,(%esp)
 4e4:	e8 37 ff ff ff       	call   420 <putc>
 4e9:	eb 01                	jmp    4ec <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 4eb:	90                   	nop
 4ec:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 4f0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 4f4:	79 d9                	jns    4cf <printint+0x87>
    putc(fd, buf[i]);
}
 4f6:	83 c4 44             	add    $0x44,%esp
 4f9:	5b                   	pop    %ebx
 4fa:	5d                   	pop    %ebp
 4fb:	c3                   	ret    

000004fc <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 4fc:	55                   	push   %ebp
 4fd:	89 e5                	mov    %esp,%ebp
 4ff:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 502:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 509:	8d 45 0c             	lea    0xc(%ebp),%eax
 50c:	83 c0 04             	add    $0x4,%eax
 50f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 512:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 519:	e9 7e 01 00 00       	jmp    69c <printf+0x1a0>
    c = fmt[i] & 0xff;
 51e:	8b 55 0c             	mov    0xc(%ebp),%edx
 521:	8b 45 ec             	mov    -0x14(%ebp),%eax
 524:	8d 04 02             	lea    (%edx,%eax,1),%eax
 527:	0f b6 00             	movzbl (%eax),%eax
 52a:	0f be c0             	movsbl %al,%eax
 52d:	25 ff 00 00 00       	and    $0xff,%eax
 532:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 535:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 539:	75 2c                	jne    567 <printf+0x6b>
      if(c == '%'){
 53b:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 53f:	75 0c                	jne    54d <printf+0x51>
        state = '%';
 541:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 548:	e9 4b 01 00 00       	jmp    698 <printf+0x19c>
      } else {
        putc(fd, c);
 54d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 550:	0f be c0             	movsbl %al,%eax
 553:	89 44 24 04          	mov    %eax,0x4(%esp)
 557:	8b 45 08             	mov    0x8(%ebp),%eax
 55a:	89 04 24             	mov    %eax,(%esp)
 55d:	e8 be fe ff ff       	call   420 <putc>
 562:	e9 31 01 00 00       	jmp    698 <printf+0x19c>
      }
    } else if(state == '%'){
 567:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 56b:	0f 85 27 01 00 00    	jne    698 <printf+0x19c>
      if(c == 'd'){
 571:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 575:	75 2d                	jne    5a4 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 577:	8b 45 f4             	mov    -0xc(%ebp),%eax
 57a:	8b 00                	mov    (%eax),%eax
 57c:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 583:	00 
 584:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 58b:	00 
 58c:	89 44 24 04          	mov    %eax,0x4(%esp)
 590:	8b 45 08             	mov    0x8(%ebp),%eax
 593:	89 04 24             	mov    %eax,(%esp)
 596:	e8 ad fe ff ff       	call   448 <printint>
        ap++;
 59b:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 59f:	e9 ed 00 00 00       	jmp    691 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 5a4:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 5a8:	74 06                	je     5b0 <printf+0xb4>
 5aa:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 5ae:	75 2d                	jne    5dd <printf+0xe1>
        printint(fd, *ap, 16, 0);
 5b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5b3:	8b 00                	mov    (%eax),%eax
 5b5:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 5bc:	00 
 5bd:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 5c4:	00 
 5c5:	89 44 24 04          	mov    %eax,0x4(%esp)
 5c9:	8b 45 08             	mov    0x8(%ebp),%eax
 5cc:	89 04 24             	mov    %eax,(%esp)
 5cf:	e8 74 fe ff ff       	call   448 <printint>
        ap++;
 5d4:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 5d8:	e9 b4 00 00 00       	jmp    691 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 5dd:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 5e1:	75 46                	jne    629 <printf+0x12d>
        s = (char*)*ap;
 5e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5e6:	8b 00                	mov    (%eax),%eax
 5e8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 5eb:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 5ef:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 5f3:	75 27                	jne    61c <printf+0x120>
          s = "(null)";
 5f5:	c7 45 e4 16 09 00 00 	movl   $0x916,-0x1c(%ebp)
        while(*s != 0){
 5fc:	eb 1f                	jmp    61d <printf+0x121>
          putc(fd, *s);
 5fe:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 601:	0f b6 00             	movzbl (%eax),%eax
 604:	0f be c0             	movsbl %al,%eax
 607:	89 44 24 04          	mov    %eax,0x4(%esp)
 60b:	8b 45 08             	mov    0x8(%ebp),%eax
 60e:	89 04 24             	mov    %eax,(%esp)
 611:	e8 0a fe ff ff       	call   420 <putc>
          s++;
 616:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 61a:	eb 01                	jmp    61d <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 61c:	90                   	nop
 61d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 620:	0f b6 00             	movzbl (%eax),%eax
 623:	84 c0                	test   %al,%al
 625:	75 d7                	jne    5fe <printf+0x102>
 627:	eb 68                	jmp    691 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 629:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 62d:	75 1d                	jne    64c <printf+0x150>
        putc(fd, *ap);
 62f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 632:	8b 00                	mov    (%eax),%eax
 634:	0f be c0             	movsbl %al,%eax
 637:	89 44 24 04          	mov    %eax,0x4(%esp)
 63b:	8b 45 08             	mov    0x8(%ebp),%eax
 63e:	89 04 24             	mov    %eax,(%esp)
 641:	e8 da fd ff ff       	call   420 <putc>
        ap++;
 646:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 64a:	eb 45                	jmp    691 <printf+0x195>
      } else if(c == '%'){
 64c:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 650:	75 17                	jne    669 <printf+0x16d>
        putc(fd, c);
 652:	8b 45 e8             	mov    -0x18(%ebp),%eax
 655:	0f be c0             	movsbl %al,%eax
 658:	89 44 24 04          	mov    %eax,0x4(%esp)
 65c:	8b 45 08             	mov    0x8(%ebp),%eax
 65f:	89 04 24             	mov    %eax,(%esp)
 662:	e8 b9 fd ff ff       	call   420 <putc>
 667:	eb 28                	jmp    691 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 669:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 670:	00 
 671:	8b 45 08             	mov    0x8(%ebp),%eax
 674:	89 04 24             	mov    %eax,(%esp)
 677:	e8 a4 fd ff ff       	call   420 <putc>
        putc(fd, c);
 67c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 67f:	0f be c0             	movsbl %al,%eax
 682:	89 44 24 04          	mov    %eax,0x4(%esp)
 686:	8b 45 08             	mov    0x8(%ebp),%eax
 689:	89 04 24             	mov    %eax,(%esp)
 68c:	e8 8f fd ff ff       	call   420 <putc>
      }
      state = 0;
 691:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 698:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 69c:	8b 55 0c             	mov    0xc(%ebp),%edx
 69f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 6a2:	8d 04 02             	lea    (%edx,%eax,1),%eax
 6a5:	0f b6 00             	movzbl (%eax),%eax
 6a8:	84 c0                	test   %al,%al
 6aa:	0f 85 6e fe ff ff    	jne    51e <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6b0:	c9                   	leave  
 6b1:	c3                   	ret    
 6b2:	90                   	nop
 6b3:	90                   	nop

000006b4 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6b4:	55                   	push   %ebp
 6b5:	89 e5                	mov    %esp,%ebp
 6b7:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6ba:	8b 45 08             	mov    0x8(%ebp),%eax
 6bd:	83 e8 08             	sub    $0x8,%eax
 6c0:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6c3:	a1 3c 09 00 00       	mov    0x93c,%eax
 6c8:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6cb:	eb 24                	jmp    6f1 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d0:	8b 00                	mov    (%eax),%eax
 6d2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6d5:	77 12                	ja     6e9 <free+0x35>
 6d7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6da:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6dd:	77 24                	ja     703 <free+0x4f>
 6df:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e2:	8b 00                	mov    (%eax),%eax
 6e4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6e7:	77 1a                	ja     703 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6e9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ec:	8b 00                	mov    (%eax),%eax
 6ee:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6f1:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6f4:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6f7:	76 d4                	jbe    6cd <free+0x19>
 6f9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6fc:	8b 00                	mov    (%eax),%eax
 6fe:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 701:	76 ca                	jbe    6cd <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 703:	8b 45 f8             	mov    -0x8(%ebp),%eax
 706:	8b 40 04             	mov    0x4(%eax),%eax
 709:	c1 e0 03             	shl    $0x3,%eax
 70c:	89 c2                	mov    %eax,%edx
 70e:	03 55 f8             	add    -0x8(%ebp),%edx
 711:	8b 45 fc             	mov    -0x4(%ebp),%eax
 714:	8b 00                	mov    (%eax),%eax
 716:	39 c2                	cmp    %eax,%edx
 718:	75 24                	jne    73e <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 71a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 71d:	8b 50 04             	mov    0x4(%eax),%edx
 720:	8b 45 fc             	mov    -0x4(%ebp),%eax
 723:	8b 00                	mov    (%eax),%eax
 725:	8b 40 04             	mov    0x4(%eax),%eax
 728:	01 c2                	add    %eax,%edx
 72a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 72d:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 730:	8b 45 fc             	mov    -0x4(%ebp),%eax
 733:	8b 00                	mov    (%eax),%eax
 735:	8b 10                	mov    (%eax),%edx
 737:	8b 45 f8             	mov    -0x8(%ebp),%eax
 73a:	89 10                	mov    %edx,(%eax)
 73c:	eb 0a                	jmp    748 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 73e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 741:	8b 10                	mov    (%eax),%edx
 743:	8b 45 f8             	mov    -0x8(%ebp),%eax
 746:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 748:	8b 45 fc             	mov    -0x4(%ebp),%eax
 74b:	8b 40 04             	mov    0x4(%eax),%eax
 74e:	c1 e0 03             	shl    $0x3,%eax
 751:	03 45 fc             	add    -0x4(%ebp),%eax
 754:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 757:	75 20                	jne    779 <free+0xc5>
    p->s.size += bp->s.size;
 759:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75c:	8b 50 04             	mov    0x4(%eax),%edx
 75f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 762:	8b 40 04             	mov    0x4(%eax),%eax
 765:	01 c2                	add    %eax,%edx
 767:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76a:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 76d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 770:	8b 10                	mov    (%eax),%edx
 772:	8b 45 fc             	mov    -0x4(%ebp),%eax
 775:	89 10                	mov    %edx,(%eax)
 777:	eb 08                	jmp    781 <free+0xcd>
  } else
    p->s.ptr = bp;
 779:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77c:	8b 55 f8             	mov    -0x8(%ebp),%edx
 77f:	89 10                	mov    %edx,(%eax)
  freep = p;
 781:	8b 45 fc             	mov    -0x4(%ebp),%eax
 784:	a3 3c 09 00 00       	mov    %eax,0x93c
}
 789:	c9                   	leave  
 78a:	c3                   	ret    

0000078b <morecore>:

static Header*
morecore(uint nu)
{
 78b:	55                   	push   %ebp
 78c:	89 e5                	mov    %esp,%ebp
 78e:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 791:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 798:	77 07                	ja     7a1 <morecore+0x16>
    nu = 4096;
 79a:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7a1:	8b 45 08             	mov    0x8(%ebp),%eax
 7a4:	c1 e0 03             	shl    $0x3,%eax
 7a7:	89 04 24             	mov    %eax,(%esp)
 7aa:	e8 51 fc ff ff       	call   400 <sbrk>
 7af:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 7b2:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 7b6:	75 07                	jne    7bf <morecore+0x34>
    return 0;
 7b8:	b8 00 00 00 00       	mov    $0x0,%eax
 7bd:	eb 22                	jmp    7e1 <morecore+0x56>
  hp = (Header*)p;
 7bf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7c2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 7c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7c8:	8b 55 08             	mov    0x8(%ebp),%edx
 7cb:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 7ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7d1:	83 c0 08             	add    $0x8,%eax
 7d4:	89 04 24             	mov    %eax,(%esp)
 7d7:	e8 d8 fe ff ff       	call   6b4 <free>
  return freep;
 7dc:	a1 3c 09 00 00       	mov    0x93c,%eax
}
 7e1:	c9                   	leave  
 7e2:	c3                   	ret    

000007e3 <malloc>:

void*
malloc(uint nbytes)
{
 7e3:	55                   	push   %ebp
 7e4:	89 e5                	mov    %esp,%ebp
 7e6:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7e9:	8b 45 08             	mov    0x8(%ebp),%eax
 7ec:	83 c0 07             	add    $0x7,%eax
 7ef:	c1 e8 03             	shr    $0x3,%eax
 7f2:	83 c0 01             	add    $0x1,%eax
 7f5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 7f8:	a1 3c 09 00 00       	mov    0x93c,%eax
 7fd:	89 45 f0             	mov    %eax,-0x10(%ebp)
 800:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 804:	75 23                	jne    829 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 806:	c7 45 f0 34 09 00 00 	movl   $0x934,-0x10(%ebp)
 80d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 810:	a3 3c 09 00 00       	mov    %eax,0x93c
 815:	a1 3c 09 00 00       	mov    0x93c,%eax
 81a:	a3 34 09 00 00       	mov    %eax,0x934
    base.s.size = 0;
 81f:	c7 05 38 09 00 00 00 	movl   $0x0,0x938
 826:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 829:	8b 45 f0             	mov    -0x10(%ebp),%eax
 82c:	8b 00                	mov    (%eax),%eax
 82e:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 831:	8b 45 ec             	mov    -0x14(%ebp),%eax
 834:	8b 40 04             	mov    0x4(%eax),%eax
 837:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 83a:	72 4d                	jb     889 <malloc+0xa6>
      if(p->s.size == nunits)
 83c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 83f:	8b 40 04             	mov    0x4(%eax),%eax
 842:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 845:	75 0c                	jne    853 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 847:	8b 45 ec             	mov    -0x14(%ebp),%eax
 84a:	8b 10                	mov    (%eax),%edx
 84c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 84f:	89 10                	mov    %edx,(%eax)
 851:	eb 26                	jmp    879 <malloc+0x96>
      else {
        p->s.size -= nunits;
 853:	8b 45 ec             	mov    -0x14(%ebp),%eax
 856:	8b 40 04             	mov    0x4(%eax),%eax
 859:	89 c2                	mov    %eax,%edx
 85b:	2b 55 f4             	sub    -0xc(%ebp),%edx
 85e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 861:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 864:	8b 45 ec             	mov    -0x14(%ebp),%eax
 867:	8b 40 04             	mov    0x4(%eax),%eax
 86a:	c1 e0 03             	shl    $0x3,%eax
 86d:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 870:	8b 45 ec             	mov    -0x14(%ebp),%eax
 873:	8b 55 f4             	mov    -0xc(%ebp),%edx
 876:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 879:	8b 45 f0             	mov    -0x10(%ebp),%eax
 87c:	a3 3c 09 00 00       	mov    %eax,0x93c
      return (void*)(p + 1);
 881:	8b 45 ec             	mov    -0x14(%ebp),%eax
 884:	83 c0 08             	add    $0x8,%eax
 887:	eb 38                	jmp    8c1 <malloc+0xde>
    }
    if(p == freep)
 889:	a1 3c 09 00 00       	mov    0x93c,%eax
 88e:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 891:	75 1b                	jne    8ae <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 893:	8b 45 f4             	mov    -0xc(%ebp),%eax
 896:	89 04 24             	mov    %eax,(%esp)
 899:	e8 ed fe ff ff       	call   78b <morecore>
 89e:	89 45 ec             	mov    %eax,-0x14(%ebp)
 8a1:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 8a5:	75 07                	jne    8ae <malloc+0xcb>
        return 0;
 8a7:	b8 00 00 00 00       	mov    $0x0,%eax
 8ac:	eb 13                	jmp    8c1 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8ae:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8b4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8b7:	8b 00                	mov    (%eax),%eax
 8b9:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8bc:	e9 70 ff ff ff       	jmp    831 <malloc+0x4e>
}
 8c1:	c9                   	leave  
 8c2:	c3                   	ret    
