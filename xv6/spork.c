#include "types.h"
#include "user.h"
#include "stat.h"


int waitForInput(char* i){
  char ebuf[512];
  char *e = gets(ebuf, 10);
  if(strcmp(e, i) == 0){
    return 1;
  } else {
    return waitForInput(i);
  }
}

int main(int argc, char** argv){
  while(1){
    waitForInput("go\n");
    int pid = fork();
    if(pid == 0){
      char *nargs[] = {};
      exec("itsaspork", nargs);
    } else {
      waitForInput("stop\n");
      kill(pid);
      wait();
      exit();
      return 0;
    }
  }
}

